# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-10-12 18:55-0700\n"
"PO-Revision-Date: 2021-08-27 11:48+0000\n"
"Last-Translator: Cube Kassaki <2524737581@qq.com>\n"
"Language-Team: Chinese (Simplified) <https://hosted.weblate.org/projects/gnu-"
"mailman/hyperkitty/zh_Hans/>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.8.1-dev\n"

#: forms.py:53
msgid "Add a tag..."
msgstr ""

#: forms.py:55
msgid "Add"
msgstr ""

#: forms.py:56
msgid "use commas to add multiple tags"
msgstr ""

#: forms.py:64
msgid "Attach a file"
msgstr "添加一个文件附件"

#: forms.py:65
msgid "Attach another file"
msgstr "再添加一个文件附件"

#: forms.py:66
msgid "Remove this file"
msgstr "删除此文件"

#: templates/hyperkitty/404.html:28
msgid "Error 404"
msgstr "错误代码404"

#: templates/hyperkitty/404.html:30 templates/hyperkitty/500.html:31
msgid "Oh No!"
msgstr "噢, 不要!"

#: templates/hyperkitty/404.html:32
msgid "I can't find this page."
msgstr "我找不到这页."

#: templates/hyperkitty/404.html:33 templates/hyperkitty/500.html:34
msgid "Go back home"
msgstr "回家"

#: templates/hyperkitty/500.html:29
msgid "Error 500"
msgstr "错误代码500"

#: templates/hyperkitty/500.html:33
msgid "Sorry, but the requested page is unavailable due to a server hiccup."
msgstr "对不起, 由于服务器故障, 请求的页面暂时不能显示."

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "started"
msgstr "已开始"

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "last active:"
msgstr "最后活跃的:"

#: templates/hyperkitty/ajax/reattach_suggest.html:8
msgid "see this thread"
msgstr "读这个线索"

#: templates/hyperkitty/ajax/reattach_suggest.html:12
msgid "(no suggestions)"
msgstr "(没有建议)"

#: templates/hyperkitty/ajax/temp_message.html:12
msgid "Sent just now, not yet distributed"
msgstr "刚发送, 尚未分发"

#: templates/hyperkitty/api.html:5
msgid "REST API"
msgstr "REST API"

#: templates/hyperkitty/api.html:7
msgid ""
"HyperKitty comes with a small REST API allowing you to programatically "
"retrieve emails and information."
msgstr "HyperKitty自带REST API能让你用程序获得电子邮件和相关信息."

#: templates/hyperkitty/api.html:10
msgid "Formats"
msgstr "格式"

#: templates/hyperkitty/api.html:12
msgid ""
"This REST API can return the information into several formats.  The default "
"format is html to allow human readibility."
msgstr "REST API能以几种格式返回信息. 缺省格式是html以便于阅读."

#: templates/hyperkitty/api.html:14
msgid ""
"To change the format, just add <em>?format=&lt;FORMAT&gt;</em> to the URL."
msgstr "想改格式的话, 只需在URL后加入<em>?format=&lt;FORMAT&gt;</em>."

#: templates/hyperkitty/api.html:16
msgid "The list of available formats is:"
msgstr "可用的格式包括:"

#: templates/hyperkitty/api.html:20
msgid "Plain text"
msgstr "纯文本"

#: templates/hyperkitty/api.html:26
msgid "List of mailing-lists"
msgstr "邮件列表的列表"

#: templates/hyperkitty/api.html:27 templates/hyperkitty/api.html:33
#: templates/hyperkitty/api.html:39 templates/hyperkitty/api.html:45
#: templates/hyperkitty/api.html:51
msgid "Endpoint:"
msgstr "端点:"

#: templates/hyperkitty/api.html:29
msgid ""
"Using this address you will be able to retrieve the information known about "
"all the mailing lists."
msgstr "你能通过这个地址获得所有邮件列表的信息."

#: templates/hyperkitty/api.html:32
msgid "Threads in a mailing list"
msgstr "邮件列表中的线索"

#: templates/hyperkitty/api.html:35
msgid ""
"Using this address you will be able to retrieve information about all the "
"threads on the specified mailing list."
msgstr "你能通过这个地址获得这个邮件列表上所有线索的相关信息."

#: templates/hyperkitty/api.html:38
msgid "Emails in a thread"
msgstr "线索中的邮件"

#: templates/hyperkitty/api.html:41
msgid ""
"Using this address you will be able to retrieve the list of emails in a "
"mailing list thread."
msgstr "你能通过这个地址获得这个邮件线索中的全部邮件的列表."

#: templates/hyperkitty/api.html:44
msgid "An email in a mailing list"
msgstr "一个邮件列表中的一封邮件"

#: templates/hyperkitty/api.html:47
msgid ""
"Using this address you will be able to retrieve the information known about "
"a specific email on the specified mailing list."
msgstr "你能通过这个地址获得此邮件列表上一封特定邮件的信息."

#: templates/hyperkitty/api.html:50
msgid "Tags"
msgstr "标签"

#: templates/hyperkitty/api.html:53
msgid "Using this address you will be able to retrieve the list of tags."
msgstr "通过这个地址你能获得所有标签."

#: templates/hyperkitty/base.html:57 templates/hyperkitty/base.html:112
msgid "Account"
msgstr "账户"

#: templates/hyperkitty/base.html:62 templates/hyperkitty/base.html:117
msgid "Mailman settings"
msgstr "Mailman的设定"

#: templates/hyperkitty/base.html:67 templates/hyperkitty/base.html:122
#: templates/hyperkitty/user_profile/base.html:17
msgid "Posting activity"
msgstr "发表活动"

#: templates/hyperkitty/base.html:72 templates/hyperkitty/base.html:127
msgid "Logout"
msgstr "退出登录"

#: templates/hyperkitty/base.html:78 templates/hyperkitty/base.html:134
msgid "Sign In"
msgstr "登录"

#: templates/hyperkitty/base.html:82 templates/hyperkitty/base.html:138
msgid "Sign Up"
msgstr "注册"

#: templates/hyperkitty/base.html:91
msgid "Search this list"
msgstr "搜索此列表"

#: templates/hyperkitty/base.html:91
msgid "Search all lists"
msgstr "搜索全部列表"

#: templates/hyperkitty/base.html:149
msgid "Manage this list"
msgstr "管理此列表"

#: templates/hyperkitty/base.html:154
msgid "Manage lists"
msgstr "管理全部列表"

#: templates/hyperkitty/base.html:192
msgid "Keyboard Shortcuts"
msgstr "快捷键"

#: templates/hyperkitty/base.html:195
msgid "Thread View"
msgstr "会话视图"

#: templates/hyperkitty/base.html:197
msgid "Next unread message"
msgstr "下一条未读消息"

#: templates/hyperkitty/base.html:198
msgid "Previous unread message"
msgstr "上一条未读消息"

#: templates/hyperkitty/base.html:199
msgid "Jump to all threads"
msgstr "跳转到全部会话"

#: templates/hyperkitty/base.html:200
msgid "Jump to MailingList overview"
msgstr "跳转到邮件列表概览"

#: templates/hyperkitty/base.html:214
msgid "Powered by"
msgstr "技术支持"

#: templates/hyperkitty/base.html:214
msgid "version"
msgstr "版本"

#: templates/hyperkitty/errors/notimplemented.html:7
msgid "Not implemented yet"
msgstr "尚未实现"

#: templates/hyperkitty/errors/notimplemented.html:12
msgid "Not implemented"
msgstr "未实现"

#: templates/hyperkitty/errors/notimplemented.html:14
msgid "This feature has not been implemented yet, sorry."
msgstr "对不起，这项功能尚未实现。"

#: templates/hyperkitty/errors/private.html:7
msgid "Error: private list"
msgstr "错误: 私有列表"

#: templates/hyperkitty/errors/private.html:19
msgid ""
"This mailing list is private. You must be subscribed to view the archives."
msgstr "本邮件列表不向公众开放. 想浏览档案需先订阅."

#: templates/hyperkitty/fragments/like_form.html:16
msgid "You like it (cancel)"
msgstr "你喜欢(取消)"

#: templates/hyperkitty/fragments/like_form.html:24
msgid "You dislike it (cancel)"
msgstr "你不喜欢(取消)"

#: templates/hyperkitty/fragments/like_form.html:27
#: templates/hyperkitty/fragments/like_form.html:31
msgid "You must be logged-in to vote."
msgstr "你必须先登录才能投票."

#: templates/hyperkitty/fragments/month_list.html:6
#, fuzzy
#| msgid "Thread"
msgid "Threads by"
msgstr "线索"

#: templates/hyperkitty/fragments/month_list.html:6
msgid " month"
msgstr " 月份"

#: templates/hyperkitty/fragments/overview_threads.html:12
msgid "New messages in this thread"
msgstr "线索中的新消息"

#: templates/hyperkitty/fragments/overview_threads.html:37
#: templates/hyperkitty/fragments/thread_left_nav.html:18
#: templates/hyperkitty/overview.html:78
msgid "All Threads"
msgstr "所有线索"

#: templates/hyperkitty/fragments/overview_top_posters.html:18
msgid "See the profile"
msgstr "看基本信息"

#: templates/hyperkitty/fragments/overview_top_posters.html:24
msgid "posts"
msgstr "贴文"

#: templates/hyperkitty/fragments/overview_top_posters.html:29
msgid "No posters this month (yet)."
msgstr "本月尚无人发贴."

#: templates/hyperkitty/fragments/send_as.html:5
msgid "This message will be sent as:"
msgstr "本消息会发送为:"

#: templates/hyperkitty/fragments/send_as.html:6
msgid "Change sender"
msgstr "更改发送人"

#: templates/hyperkitty/fragments/send_as.html:16
msgid "Link another address"
msgstr "再链接一个地址"

#: templates/hyperkitty/fragments/send_as.html:20
msgid ""
"If you aren't a current list member, sending this message will subscribe you."
msgstr "如果您不是当前列表成员，发送此消息将您订阅。"

#: templates/hyperkitty/fragments/thread_left_nav.html:11
msgid "List overview"
msgstr "列表概述"

#: templates/hyperkitty/fragments/thread_left_nav.html:27 views/message.py:75
#: views/mlist.py:102 views/thread.py:167
msgid "Download"
msgstr "下载"

#: templates/hyperkitty/fragments/thread_left_nav.html:30
msgid "Past 30 days"
msgstr "过去30天"

#: templates/hyperkitty/fragments/thread_left_nav.html:31
msgid "This month"
msgstr "这个月"

#: templates/hyperkitty/fragments/thread_left_nav.html:34
msgid "Entire archive"
msgstr "全部档案"

#: templates/hyperkitty/index.html:9 templates/hyperkitty/index.html:63
msgid "Available lists"
msgstr "可选列表"

#: templates/hyperkitty/index.html:22 templates/hyperkitty/index.html:27
#: templates/hyperkitty/index.html:72
msgid "Most popular"
msgstr "最流行的"

#: templates/hyperkitty/index.html:26
msgid "Sort by number of recent participants"
msgstr "由近期参与人数排序"

#: templates/hyperkitty/index.html:32 templates/hyperkitty/index.html:37
#: templates/hyperkitty/index.html:75
msgid "Most active"
msgstr "最活跃的"

#: templates/hyperkitty/index.html:36
msgid "Sort by number of recent discussions"
msgstr "由最近讨论数量排序"

#: templates/hyperkitty/index.html:42 templates/hyperkitty/index.html:47
#: templates/hyperkitty/index.html:78
msgid "By name"
msgstr "以名字"

#: templates/hyperkitty/index.html:46
msgid "Sort alphabetically"
msgstr "字典排序"

#: templates/hyperkitty/index.html:52 templates/hyperkitty/index.html:57
#: templates/hyperkitty/index.html:81
msgid "Newest"
msgstr "最新的"

#: templates/hyperkitty/index.html:56
msgid "Sort by list creation date"
msgstr "由列表创建日期排序"

#: templates/hyperkitty/index.html:68
msgid "Sort by"
msgstr "排序以"

#: templates/hyperkitty/index.html:91
msgid "Hide inactive"
msgstr "隐藏非活跃的"

#: templates/hyperkitty/index.html:92
msgid "Hide private"
msgstr "隐藏私有的"

#: templates/hyperkitty/index.html:99
msgid "Find list"
msgstr "找列表"

#: templates/hyperkitty/index.html:123 templates/hyperkitty/index.html:193
#: templates/hyperkitty/user_profile/last_views.html:34
#: templates/hyperkitty/user_profile/last_views.html:73
msgid "new"
msgstr "新的"

#: templates/hyperkitty/index.html:134 templates/hyperkitty/index.html:204
msgid "private"
msgstr "私有"

#: templates/hyperkitty/index.html:136 templates/hyperkitty/index.html:206
msgid "inactive"
msgstr "非活跃的"

#: templates/hyperkitty/index.html:142 templates/hyperkitty/index.html:232
#: templates/hyperkitty/overview.html:94 templates/hyperkitty/overview.html:111
#: templates/hyperkitty/overview.html:181
#: templates/hyperkitty/overview.html:188
#: templates/hyperkitty/overview.html:195
#: templates/hyperkitty/overview.html:204
#: templates/hyperkitty/overview.html:212 templates/hyperkitty/reattach.html:39
#: templates/hyperkitty/thread.html:111
msgid "Loading..."
msgstr "正在加载..."

#: templates/hyperkitty/index.html:148 templates/hyperkitty/index.html:221
#: templates/hyperkitty/overview.html:103
#: templates/hyperkitty/thread_list.html:40
#: templates/hyperkitty/threads/right_col.html:97
#: templates/hyperkitty/threads/summary_thread_large.html:47
msgid "participants"
msgstr "参与者"

#: templates/hyperkitty/index.html:153 templates/hyperkitty/index.html:226
#: templates/hyperkitty/overview.html:104
#: templates/hyperkitty/thread_list.html:45
msgid "discussions"
msgstr "讨论"

#: templates/hyperkitty/index.html:162 templates/hyperkitty/index.html:240
msgid "No archived list yet."
msgstr "尚未有归档的列表."

#: templates/hyperkitty/index.html:174
#: templates/hyperkitty/user_profile/favorites.html:40
#: templates/hyperkitty/user_profile/last_views.html:45
#: templates/hyperkitty/user_profile/profile.html:15
#: templates/hyperkitty/user_profile/subscriptions.html:41
#: templates/hyperkitty/user_profile/votes.html:46
msgid "List"
msgstr "列表"

#: templates/hyperkitty/index.html:175
msgid "Description"
msgstr "描述"

#: templates/hyperkitty/index.html:176
msgid "Activity in the past 30 days"
msgstr "过去30天的活动"

#: templates/hyperkitty/list_delete.html:7
msgid "Delete MailingList"
msgstr "删除邮件列表"

#: templates/hyperkitty/list_delete.html:20
msgid "Delete Mailing List"
msgstr "删除邮件列表"

#: templates/hyperkitty/list_delete.html:26
msgid ""
"will be deleted along with all the threads and messages. Do you want to "
"continue?"
msgstr "将与所有主题和消息一起删除。 你要继续吗？"

#: templates/hyperkitty/list_delete.html:33
#: templates/hyperkitty/message_delete.html:44
msgid "Delete"
msgstr "删除"

#: templates/hyperkitty/list_delete.html:34
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
msgid "or"
msgstr "或"

#: templates/hyperkitty/list_delete.html:36
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
#: templates/hyperkitty/user_profile/votes.html:36
#: templates/hyperkitty/user_profile/votes.html:74
msgid "cancel"
msgstr "中止"

#: templates/hyperkitty/message.html:22
msgid "thread"
msgstr "线索"

#: templates/hyperkitty/message_delete.html:7
#: templates/hyperkitty/message_delete.html:20
msgid "Delete message(s)"
msgstr "删除消息"

#: templates/hyperkitty/message_delete.html:25
#, python-format
msgid ""
"\n"
"        %(count)s message(s) will be deleted. Do you want to continue?\n"
"        "
msgstr ""
"\n"
"        %(count)s个消息将被删除. 是否继续?\n"
"        "

#: templates/hyperkitty/message_new.html:8
#: templates/hyperkitty/message_new.html:21
msgid "Create a new thread"
msgstr "创建一个线索"

#: templates/hyperkitty/message_new.html:22
#: templates/hyperkitty/user_posts.html:22
msgid "in"
msgstr "内"

#: templates/hyperkitty/message_new.html:52
#: templates/hyperkitty/messages/message.html:145
msgid "Send"
msgstr "发送"

#: templates/hyperkitty/messages/message.html:18
#, python-format
msgid "See the profile for %(name)s"
msgstr "看%(name)s 的基本信息"

#: templates/hyperkitty/messages/message.html:28
msgid "Unread"
msgstr "未读"

#: templates/hyperkitty/messages/message.html:45
msgid "Sender's time:"
msgstr "发送者的时间:"

#: templates/hyperkitty/messages/message.html:51
msgid "New subject:"
msgstr "新主题:"

#: templates/hyperkitty/messages/message.html:61
msgid "Attachments:"
msgstr "附件:"

#: templates/hyperkitty/messages/message.html:76
msgid "Display in fixed font"
msgstr "用定宽字体显示"

#: templates/hyperkitty/messages/message.html:79
msgid "Permalink for this message"
msgstr "本消息的永久链接"

#: templates/hyperkitty/messages/message.html:90
#: templates/hyperkitty/messages/message.html:96
msgid "Reply"
msgstr "回复"

#: templates/hyperkitty/messages/message.html:93
msgid "Sign in to reply online"
msgstr "登录后在线回复"

#: templates/hyperkitty/messages/message.html:105
#, python-format
msgid ""
"\n"
"                %(email.attachments.count)s attachment\n"
"                "
msgid_plural ""
"\n"
"                %(email.attachments.count)s attachments\n"
"                "
msgstr[0] ""
"\n"
"                %(email.attachments.count)s个附件\n"
"                "

#: templates/hyperkitty/messages/message.html:131
msgid "Quote"
msgstr "引用"

#: templates/hyperkitty/messages/message.html:132
msgid "Create new thread"
msgstr "创建新线索"

#: templates/hyperkitty/messages/message.html:135
msgid "Use email software"
msgstr "用邮件软件"

#: templates/hyperkitty/messages/right_col.html:11
msgid "Back to the thread"
msgstr "回到线索"

#: templates/hyperkitty/messages/right_col.html:18
msgid "Back to the list"
msgstr "回到列表"

#: templates/hyperkitty/messages/right_col.html:27
msgid "Delete this message"
msgstr "删除此消息"

#: templates/hyperkitty/messages/summary_message.html:23
#, python-format
msgid ""
"\n"
"                                by %(name)s\n"
"                            "
msgstr ""
"\n"
"                                %(name)s所写\n"
"                            "

#: templates/hyperkitty/overview.html:38
msgid "Home"
msgstr "主页"

#: templates/hyperkitty/overview.html:41 templates/hyperkitty/thread.html:78
msgid "Stats"
msgstr "统计"

#: templates/hyperkitty/overview.html:44
#, fuzzy
#| msgid "Thread"
msgid "Threads"
msgstr "线索"

#: templates/hyperkitty/overview.html:50 templates/hyperkitty/overview.html:61
#: templates/hyperkitty/thread_list.html:48
msgid "You must be logged-in to create a thread."
msgstr "你登录后才能创建线索."

#: templates/hyperkitty/overview.html:63
#: templates/hyperkitty/thread_list.html:52
#, fuzzy
#| msgid ""
#| "<span class=\"hidden-tn hidden-xs\">Start a </span><span class=\"hidden-"
#| "sm hidden-md hidden-lg\">N</span>ew thread"
msgid ""
"<span class=\"d-none d-md-inline\">Start a n</span><span class=\"d-md-none"
"\">N</span>ew thread"
msgstr ""
"<span class=\"hidden-tn hidden-xs\">启用一个</span><span class=\"hidden-sm "
"hidden-md hidden-lg\"></span>新线索"

#: templates/hyperkitty/overview.html:75
#, fuzzy
#| msgid ""
#| "<span class=\"hidden-tn hidden-xs\">Manage s</span><span class=\"hidden-"
#| "sm hidden-md hidden-lg\">S</span>ubscription"
msgid ""
"<span class=\"d-none d-md-inline\">Manage s</span><span class=\"d-md-none"
"\">S</span>ubscription"
msgstr ""
"<span class=\"hidden-tn hidden-xs\">管理</span><span class=\"hidden-sm "
"hidden-md hidden-lg\"></span>订阅"

#: templates/hyperkitty/overview.html:81
#, fuzzy
#| msgid "Entire archive"
msgid "Delete Archive"
msgstr "全部档案"

#: templates/hyperkitty/overview.html:91
msgid "Activity Summary"
msgstr "活动概述"

#: templates/hyperkitty/overview.html:93
msgid "Post volume over the past <strong>30</strong> days."
msgstr "过去<strong>30</strong>天的发贴数."

#: templates/hyperkitty/overview.html:98
msgid "The following statistics are from"
msgstr "之后的统计数据来自"

#: templates/hyperkitty/overview.html:99
msgid "In"
msgstr "在"

#: templates/hyperkitty/overview.html:100
msgid "the past <strong>30</strong> days:"
msgstr "过去<strong>30</strong>天:"

#: templates/hyperkitty/overview.html:109
msgid "Most active posters"
msgstr "最活跃的发帖人"

#: templates/hyperkitty/overview.html:118
msgid "Prominent posters"
msgstr "著名发帖人"

#: templates/hyperkitty/overview.html:133
msgid "kudos"
msgstr "感谢"

#: templates/hyperkitty/overview.html:152
msgid "Recent"
msgstr ""

#: templates/hyperkitty/overview.html:156
#, fuzzy
#| msgid "Most active"
msgid "Most Active"
msgstr "最活跃的"

#: templates/hyperkitty/overview.html:160
#, fuzzy
#| msgid "Most popular"
msgid "Most Popular"
msgstr "最流行的"

#: templates/hyperkitty/overview.html:166
#: templates/hyperkitty/user_profile/base.html:22
msgid "Favorites"
msgstr "喜好"

#: templates/hyperkitty/overview.html:170
msgid "Posted"
msgstr ""

#: templates/hyperkitty/overview.html:179
msgid "Recently active discussions"
msgstr "最近活跃的讨论"

#: templates/hyperkitty/overview.html:186
msgid "Most popular discussions"
msgstr "最流行的讨论"

#: templates/hyperkitty/overview.html:193
msgid "Most active discussions"
msgstr "最活跃的讨论"

#: templates/hyperkitty/overview.html:200
msgid "Discussions You've Flagged"
msgstr "你标记的讨论"

#: templates/hyperkitty/overview.html:208
msgid "Discussions You've Posted to"
msgstr "你参与的讨论"

#: templates/hyperkitty/reattach.html:9
msgid "Reattach a thread"
msgstr "接回线索"

#: templates/hyperkitty/reattach.html:20
msgid "Re-attach a thread to another"
msgstr "把一个线索接到另一个上"

#: templates/hyperkitty/reattach.html:22
msgid "Thread to re-attach:"
msgstr "要接回的线索:"

#: templates/hyperkitty/reattach.html:29
msgid "Re-attach it to:"
msgstr "把它接到:"

#: templates/hyperkitty/reattach.html:31
msgid "Search for the parent thread"
msgstr "搜索上级线索"

#: templates/hyperkitty/reattach.html:32
msgid "Search"
msgstr "搜索"

#: templates/hyperkitty/reattach.html:44
msgid "this thread ID:"
msgstr "这个线索号:"

#: templates/hyperkitty/reattach.html:50
msgid "Do it"
msgstr "做吧"

#: templates/hyperkitty/reattach.html:50
msgid "(there's no undoing!), or"
msgstr "(无法撤回!), 或者"

#: templates/hyperkitty/reattach.html:52
msgid "go back to the thread"
msgstr "回到线索"

#: templates/hyperkitty/search_results.html:8
msgid "Search results for"
msgstr "搜索结果属于"

#: templates/hyperkitty/search_results.html:30
msgid "search results"
msgstr "搜索结果"

#: templates/hyperkitty/search_results.html:32
msgid "Search results"
msgstr "搜索结果"

#: templates/hyperkitty/search_results.html:34
msgid "for query"
msgstr "为查询"

#: templates/hyperkitty/search_results.html:44
#: templates/hyperkitty/user_posts.html:34
msgid "messages"
msgstr "消息"

#: templates/hyperkitty/search_results.html:57
msgid "sort by score"
msgstr "以打分排序"

#: templates/hyperkitty/search_results.html:60
msgid "sort by latest first"
msgstr "以从新到旧排序"

#: templates/hyperkitty/search_results.html:63
msgid "sort by earliest first"
msgstr "以从旧到新排序"

#: templates/hyperkitty/search_results.html:84
msgid "Sorry no email could be found for this query."
msgstr "对不起, 找不到相应的邮件."

#: templates/hyperkitty/search_results.html:87
msgid "Sorry but your query looks empty."
msgstr "对不起, 你的查询为空."

#: templates/hyperkitty/search_results.html:88
msgid "these are not the messages you are looking for"
msgstr "这些不是你要找的消息"

#: templates/hyperkitty/thread.html:30
msgid "newer"
msgstr "较新的"

#: templates/hyperkitty/thread.html:44
msgid "older"
msgstr "较旧的"

#: templates/hyperkitty/thread.html:72
msgid "First Post"
msgstr "第一贴"

#: templates/hyperkitty/thread.html:75
#: templates/hyperkitty/user_profile/favorites.html:45
#: templates/hyperkitty/user_profile/last_views.html:50
msgid "Replies"
msgstr "回复"

#: templates/hyperkitty/thread.html:97
msgid "Show replies by thread"
msgstr "显示线索内回复"

#: templates/hyperkitty/thread.html:100
msgid "Show replies by date"
msgstr "显示某日回复"

#: templates/hyperkitty/thread_list.html:60
msgid "Sorry no email threads could be found"
msgstr "对不起, 找不到你要的邮件线索"

#: templates/hyperkitty/threads/category.html:7
msgid "Click to edit"
msgstr "点击编辑"

#: templates/hyperkitty/threads/category.html:9
msgid "You must be logged-in to edit."
msgstr "你必须先登录才能编辑."

#: templates/hyperkitty/threads/category.html:15
msgid "no category"
msgstr "没有组别"

#: templates/hyperkitty/threads/right_col.html:12
msgid "days inactive"
msgstr "不活跃天数"

#: templates/hyperkitty/threads/right_col.html:18
msgid "days old"
msgstr "活的天数"

#: templates/hyperkitty/threads/right_col.html:40
#, python-format
msgid "%(num_comments)s comments"
msgstr ""

#: templates/hyperkitty/threads/right_col.html:44
#, python-format
msgid "%(thread.participants_count)s participants"
msgstr ""

#: templates/hyperkitty/threads/right_col.html:49
#, python-format
msgid "%(unread_count)s unread <span class=\"hidden-sm\">messages</span>"
msgstr ""

#: templates/hyperkitty/threads/right_col.html:59
msgid "You must be logged-in to have favorites."
msgstr "你必须先登录才能设定喜好."

#: templates/hyperkitty/threads/right_col.html:60
msgid "Add to favorites"
msgstr "加入喜好"

#: templates/hyperkitty/threads/right_col.html:62
msgid "Remove from favorites"
msgstr "从喜好中去除"

#: templates/hyperkitty/threads/right_col.html:71
msgid "Reattach this thread"
msgstr "重连线索"

#: templates/hyperkitty/threads/right_col.html:75
msgid "Delete this thread"
msgstr "删除线索"

#: templates/hyperkitty/threads/right_col.html:113
msgid "Unreads:"
msgstr "未读:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "Go to:"
msgstr "前往:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "next"
msgstr "下一个"

#: templates/hyperkitty/threads/right_col.html:116
msgid "prev"
msgstr "上一个"

#: templates/hyperkitty/threads/summary_thread_large.html:21
#: templates/hyperkitty/threads/summary_thread_large.html:23
msgid "Favorite"
msgstr "喜好"

#: templates/hyperkitty/threads/summary_thread_large.html:29
#, python-format
msgid ""
"\n"
"                    by %(name)s\n"
"                    "
msgstr ""
"\n"
"                    撰写人 %(name)s\n"
"                    "

#: templates/hyperkitty/threads/summary_thread_large.html:39
msgid "Most recent thread activity"
msgstr "最近线索内活动"

#: templates/hyperkitty/threads/summary_thread_large.html:52
msgid "comments"
msgstr "评论"

#: templates/hyperkitty/threads/tags.html:3
msgid "tags"
msgstr "标签"

#: templates/hyperkitty/threads/tags.html:9
msgid "Search for tag"
msgstr "搜索标签"

#: templates/hyperkitty/threads/tags.html:15
msgid "Remove"
msgstr "去掉"

#: templates/hyperkitty/user_posts.html:8
#: templates/hyperkitty/user_posts.html:21
#: templates/hyperkitty/user_posts.html:25
msgid "Messages by"
msgstr "消息来自"

#: templates/hyperkitty/user_posts.html:38
#, python-format
msgid "Back to %(fullname)s's profile"
msgstr "回到%(fullname)s的用户信息"

#: templates/hyperkitty/user_posts.html:48
msgid "Sorry no email could be found by this user."
msgstr "对不起, 找不到该用户的邮件."

#: templates/hyperkitty/user_profile/base.html:5
#: templates/hyperkitty/user_profile/base.html:12
msgid "User posting activity"
msgstr "用户的发帖活动"

#: templates/hyperkitty/user_profile/base.html:12
#: templates/hyperkitty/user_public_profile.html:7
#: templates/hyperkitty/user_public_profile.html:14
msgid "for"
msgstr "为了"

#: templates/hyperkitty/user_profile/base.html:26
msgid "Threads you have read"
msgstr "你读过的线索"

#: templates/hyperkitty/user_profile/base.html:30
#: templates/hyperkitty/user_profile/profile.html:18
#: templates/hyperkitty/user_profile/subscriptions.html:45
msgid "Votes"
msgstr "投票"

#: templates/hyperkitty/user_profile/base.html:34
msgid "Subscriptions"
msgstr "订阅"

#: templates/hyperkitty/user_profile/favorites.html:24
#: templates/hyperkitty/user_profile/last_views.html:27
#: templates/hyperkitty/user_profile/votes.html:23
msgid "Original author:"
msgstr "原作者:"

#: templates/hyperkitty/user_profile/favorites.html:26
#: templates/hyperkitty/user_profile/last_views.html:29
#: templates/hyperkitty/user_profile/votes.html:25
msgid "Started on:"
msgstr "开始于:"

#: templates/hyperkitty/user_profile/favorites.html:28
#: templates/hyperkitty/user_profile/last_views.html:31
msgid "Last activity:"
msgstr "最后的活动:"

#: templates/hyperkitty/user_profile/favorites.html:30
#: templates/hyperkitty/user_profile/last_views.html:33
msgid "Replies:"
msgstr "回复:"

#: templates/hyperkitty/user_profile/favorites.html:41
#: templates/hyperkitty/user_profile/last_views.html:46
#: templates/hyperkitty/user_profile/profile.html:16
#: templates/hyperkitty/user_profile/votes.html:47
msgid "Subject"
msgstr "主题"

#: templates/hyperkitty/user_profile/favorites.html:42
#: templates/hyperkitty/user_profile/last_views.html:47
#: templates/hyperkitty/user_profile/votes.html:48
msgid "Original author"
msgstr "原作者"

#: templates/hyperkitty/user_profile/favorites.html:43
#: templates/hyperkitty/user_profile/last_views.html:48
#: templates/hyperkitty/user_profile/votes.html:49
msgid "Start date"
msgstr "开始日期"

#: templates/hyperkitty/user_profile/favorites.html:44
#: templates/hyperkitty/user_profile/last_views.html:49
msgid "Last activity"
msgstr "最后的活动"

#: templates/hyperkitty/user_profile/favorites.html:71
msgid "No favorites yet."
msgstr "还没有喜好."

#: templates/hyperkitty/user_profile/last_views.html:22
#: templates/hyperkitty/user_profile/last_views.html:59
msgid "New comments"
msgstr "新评论"

#: templates/hyperkitty/user_profile/last_views.html:82
msgid "Nothing read yet."
msgstr "全部未读."

#: templates/hyperkitty/user_profile/profile.html:9
msgid "Last posts"
msgstr "最后的发帖"

#: templates/hyperkitty/user_profile/profile.html:17
msgid "Date"
msgstr "日期"

#: templates/hyperkitty/user_profile/profile.html:19
msgid "Thread"
msgstr "线索"

#: templates/hyperkitty/user_profile/profile.html:20
msgid "Last thread activity"
msgstr "最后的线索活动"

#: templates/hyperkitty/user_profile/profile.html:49
msgid "No posts yet."
msgstr "尚无发帖."

#: templates/hyperkitty/user_profile/subscriptions.html:24
msgid "since first post"
msgstr "从第一贴开始"

#: templates/hyperkitty/user_profile/subscriptions.html:26
#: templates/hyperkitty/user_profile/subscriptions.html:63
msgid "post"
msgstr "发帖"

#: templates/hyperkitty/user_profile/subscriptions.html:31
#: templates/hyperkitty/user_profile/subscriptions.html:69
msgid "no post yet"
msgstr "尚无发帖"

#: templates/hyperkitty/user_profile/subscriptions.html:42
msgid "Time since the first activity"
msgstr "从第一个活动到现在的时间"

#: templates/hyperkitty/user_profile/subscriptions.html:43
msgid "First post"
msgstr "第一贴"

#: templates/hyperkitty/user_profile/subscriptions.html:44
msgid "Posts to this list"
msgstr "对本列表的发帖"

#: templates/hyperkitty/user_profile/subscriptions.html:76
msgid "no subscriptions"
msgstr "没有订阅"

#: templates/hyperkitty/user_profile/votes.html:32
#: templates/hyperkitty/user_profile/votes.html:70
msgid "You like it"
msgstr "你喜欢它"

#: templates/hyperkitty/user_profile/votes.html:34
#: templates/hyperkitty/user_profile/votes.html:72
msgid "You dislike it"
msgstr "你不喜欢它"

#: templates/hyperkitty/user_profile/votes.html:50
msgid "Vote"
msgstr "投票"

#: templates/hyperkitty/user_profile/votes.html:83
msgid "No vote yet."
msgstr "尚无投票."

#: templates/hyperkitty/user_public_profile.html:7
msgid "User Profile"
msgstr "用户信息"

#: templates/hyperkitty/user_public_profile.html:14
msgid "User profile"
msgstr "用户信息"

#: templates/hyperkitty/user_public_profile.html:23
msgid "Name:"
msgstr "名字:"

#: templates/hyperkitty/user_public_profile.html:28
msgid "Creation:"
msgstr "创建:"

#: templates/hyperkitty/user_public_profile.html:33
msgid "Votes for this user:"
msgstr "本用户的投票:"

#: templates/hyperkitty/user_public_profile.html:41
msgid "Email addresses:"
msgstr "电子邮件地址:"

#: views/message.py:76
msgid "This message in gzipped mbox format"
msgstr "gzip后的mbox格式下的本消息"

#: views/message.py:201
msgid "Your reply has been sent and is being processed."
msgstr "您的回复已发送，正在处理中。"

#: views/message.py:205
msgid ""
"\n"
"  You have been subscribed to {} list."
msgstr ""
"\n"
"  你已订阅了 {} 列表。"

#: views/message.py:288
#, python-format
msgid "Could not delete message %(msg_id_hash)s: %(error)s"
msgstr "无法删除消息%(msg_id_hash)s: %(error)s"

#: views/message.py:297
#, python-format
msgid "Successfully deleted %(count)s messages."
msgstr "顺利删除%(count)s个消息."

#: views/mlist.py:88
msgid "for this month"
msgstr "本月的"

#: views/mlist.py:91
msgid "for this day"
msgstr "本日的"

#: views/mlist.py:103
msgid "This month in gzipped mbox format"
msgstr "gzip后的mbox格式下本月全部"

#: views/mlist.py:200 views/mlist.py:224
msgid "No discussions this month (yet)."
msgstr "本月尚无讨论."

#: views/mlist.py:212
msgid "No vote has been cast this month (yet)."
msgstr "本月尚无投票行动."

#: views/mlist.py:241
msgid "You have not flagged any discussions (yet)."
msgstr "你尚未标记任何讨论."

#: views/mlist.py:264
msgid "You have not posted to this list (yet)."
msgstr "你尚未对本列表发帖."

#: views/mlist.py:352
msgid "You must be a staff member to delete a MailingList"
msgstr "您必须是工作人员才能删除邮件列表"

#: views/mlist.py:366
msgid "Successfully deleted {}"
msgstr "成功删除了 {}"

#: views/search.py:115
#, python-format
msgid "Parsing error: %(error)s"
msgstr "语意分析错误: %(error)s"

#: views/thread.py:168
msgid "This thread in gzipped mbox format"
msgstr "gzip后的mbox格式下的本线索"

#~ msgid "unread"
#~ msgstr "未读"

#~ msgid "Go to"
#~ msgstr "转向"

#~ msgid "More..."
#~ msgstr "更多..."

#~ msgid "Discussions"
#~ msgstr "讨论"

#~ msgid "most recent"
#~ msgstr "最新的"

#~ msgid "most popular"
#~ msgstr "最流行的"

#~ msgid "most active"
#~ msgstr "最活跃的"

#~ msgid "Update"
#~ msgstr "更新"

#, python-format
#~ msgid ""
#~ "\n"
#~ "                                        by %(name)s\n"
#~ "                                    "
#~ msgstr ""
#~ "\n"
#~ "                                        由%(name)s\n"
#~ "                                    "
